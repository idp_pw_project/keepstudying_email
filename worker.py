import pika
import time
import smtplib, ssl

port = 465  # For SSL
smtp_server = "smtp.gmail.com"
sender_email = "keep.studying.pweb@gmail.com"  # Enter your address
password = "Parolica1234"

# try:
#     _create_unverified_https_context = ssl._create_unverified_context
# except AttributeError:
#     # Legacy Python that doesn't verify HTTPS certificates by default
#     pass
# else:
#     # Handle target environment that doesn't support HTTPS verification
#     ssl._create_default_https_context = _create_unverified_https_context

time.sleep(30)

print("Worker started")
credentials = pika.PlainCredentials('username', 'password')
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='rabbitmq', credentials=credentials))
channel = connection.channel()
channel.queue_declare(queue='myQueue', durable=True)

print("Worker connected")

def callback(ch, method, properties, body):
    cmd = body.decode()
    print("Received %s" % cmd)
    receiver_email = cmd.split(", ")[1].split("!")[0]
    
    context = ssl.create_default_context()
    context.check_hostname = False
    context.verify_mode = ssl.CERT_NONE
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email, cmd)       
    print("Successfully sent email")

    ch.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='myQueue', on_message_callback=callback)
channel.start_consuming()
